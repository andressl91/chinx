#!/bin/bash

export CROSS_ENVIRONMENT_ROOT=INSTALL_PATH
export UTILITIES_DIR=${CROSS_ENVIRONMENT_ROOT}/utilities
export CROSS_COMPILE_BIN=${CROSS_ENVIRONMENT_ROOT}/bin


export ARCHITECTURE="aarch64-unknown-linux-gnu"

export PATH=${CROSS_COMPILE_BIN}:$PATH

# TODO: Use sed to make custom chinx.sh 
#sed -e "s|TARGET_ROOT|$TARGET_ROOT|g" ./pacman_support/pacman_env_var.conf > ./pacman_support/foo.config

function cmake {
    command cmake -DCMAKE_TOOLCHAIN_FILE=${UTILITIES_DIR}/cross_arm64.cmake $*
}


