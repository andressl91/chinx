###### Link Compile Path ##############

# Use with mkdir build && cd build $$ cmake .. -DCMAKE_TOOLCHAIN_FILE=/path/to/this_file

#ASSUMES FOLLOWING VARIABLES FROM chinx.sh is defined
set(TARGET_ROOT $ENV{TARGET_ROOT})
message( " TARGET ROOT IS is $ENV{TARGET_ROOT}" )
set(ARCHITECTURE $ENV{ARCHITECTURE})
set(CROSS_ENVIRONMENT_ROOT $ENV{CROSS_ENVIRONMENT_ROOT})


set(CMAKE_FIND_ROOT_PATH $ENV{TARGET_ROOT}) 
# CMAKE_FIND_ROOT_PATH configures:
# find_package(), find_program(), find_library(), find_file(), and find_path()
message( "CMAKE_FIND_ROOT_PATH is ${CMAKE_FIND_ROOT_PATH}" )

# To add another location to be searched, use:
# - find_package(), find_program(), find_library(), find_file(), and find_path()
# - IMPORTANT!!  CMAKE_PREFIX_PATX ony search /lib, /include, NOT usr/lib usr/include
set(CMAKE_PREFIX_PATH $ENV{TARGET_ROOT}/usr) 

# If the user wants to use another root/dir for finding lib/include he/she can:
# 1. Redefine CMAKE_FIND_ROOT_PATH
# 2. Redefine CMAKE_PREFIX_PATH (can be a list of locations to searched. 
#    - add more locations with semiconlon separated list


# To make sure cmake only finds lib/include for the target system one can:
# 1. Point CMAKE_FIND_ROOT_PATH to the target root
# 2. ENABLE TARGET_ROOT_ONLY FLAG to ON, cmake will only search in the TARGET_ROOT
#    - usr/lib, /usr/inclide, lib, include
# 3. If one want to reach other packages WITHIN CMAKE_FIND_ROOT_PATH, one can add 
#    CMAKE_PREFIX_PATH to folders within, f.eks. to a folder maintained by a packet manager.
#    - set(CMAKE_PREFIX_PATH $ENV{TARGET_ROOT}/pacman_root/usr) #; 

# AVOID DYNAMIC LIBRARY TEST FOR LINKER, WILL AUTOMATICLY FAIL  
# cannot execute binary file: Exec format error

# CMAKE_FORCE_"LANGUAGE"_COMPILER WILL OVERRUN, BUT UGLY
set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY")


##### CROSS-COMPILER BINARIES
set(CROSS_ENVIRONMENT_ROOT $ENV{CROSS_ENVIRONMENT_ROOT})
set(CROSS_ARM_BIN      "${CROSS_ENVIRONMENT_ROOT}/bin") 
set(CMAKE_C_COMPILER   "${CROSS_ARM_BIN}/${ARCHITECTURE}-gcc")
set(CMAKE_CXX_COMPILER "${CROSS_ARM_BIN}/${ARCHITECTURE}-g++")
set(CMAKE_AR           "${CROSS_ARM_BIN}/${ARCHITECTURE}-ar")
set(CMAKE_RANLIB       "${CROSS_ARM_BIN}/${ARCHITECTURE}-ranlib")
set(CMAKE_LINKER       "${CROSS_ARM_BIN}/${ARCHITECTURE}-ld")

# Overrun intsall prefix
# set(CMAKE_INSTALL_PREFIX ${SOME_DIR})


########### Macro to Limit Find path in host and targert ###################
option(TARGET_ROOT_ONLY "Search only for lib, include, package in CMAKE_FIND_ROOT_DIR" ON)
# SEEMS LIKE TARGET_ROOT_ONLY ALOWS TO SEARCH FOR LIB, INCLUDg DEEPER IN CMAKE_FIND_ROOT
# Seems like TARGET_ROOT_ONLY alows searching for lib/include withing CMAKE_FIND_ROOT
# EXAMPLE, setting CMAKE_PREFIX_PATH to $ENV{TARGET_ROOT}/pacman_root/usr is allowed with flag on

# for libraries and headers in the target directories
# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
if (TARGET_ROOT_ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
endif(TARGET_ROOT_ONLY)

option(ALTERNATIVE_SEARCH_LOCATIONS "Search only for lib, include, packages in alternative directories" OFF)
if (ALTERNATIVE_SEARCH_LOCATIONS)
    set(TARGET_STD_USR_PATH ${SOME_DIR}/usr)
    set(TARGET_STD_LIB_PATH ${SOME_DIR}/pacman_root/usr/lib)
    set(TARGET_SYS_LIB_PATH ${SOME_DIR}/lib)
    set(TARGET_STD_INC_PATH ${SOME_DIR}/pacman_root/usr/include)

    # Can add multiple locations with comma separated list
    set(CMAKE_INCLUDE_PATH  ${TARGET_STD_INC_PATH} ; ${TARGET_STD_INC_PATH})
    set(CMAKE_LIBRARY_PATH  ${TARGET_SYS_LIB_PATH} ; ${TARGET_STD_LIB_PATH}; ${TARGET_STD_LIB_PATH} ; ${TARGET_SYS_LIB_PATH})
    set(CMAKE_PROGRAM_PATH  ${SOME_DIR}/usr/bin})
endif(ALTERNATIVE_SEARCH_LOCATIONS)

