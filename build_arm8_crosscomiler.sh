#! /bin/bash
set -e
trap 'previous_command=$this_command; this_command=$BASH_COMMAND' DEBUG
trap 'echo FAILED COMMAND: $previous_command' EXIT

#-------------------------------------------------------------------------------------------
# This script will download packages for, configure, build and install a GCC cross-compiler.
# Customize the variables (INSTALL_PATH, TARGET, etc.) to your liking before running.
# If you get an error and need to resume the script from some point in the middle,
# just delete/comment the preceding lines before running it again.
#
# See: http://preshing.com/20141119/how-to-build-a-gcc-cross-compiler
#-------------------------------------------------------------------------------------------

CHINX_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TARGET=aarch64-unknown-linux-gnu
TARGET_ARCHITECTURE=armv8-a
INSTALL_PATH=$HOME/crossarm64/${TARGET}
TARGET_ROOT=${INSTALL_PATH}/${TARGET}
DEPENDENCIES_PATH=$INSTALL_PATH/dependencies
LINUX_ARCH=arm64
CONFIGURATION_OPTIONS="--disable-multilib" # --disable-threads --disable-shared
BINUTILS_VERSION=binutils-2.32
GCC_VERSION=gcc-9.1.0 # error in final step, might be due to bleeding edge GCC
LINUX_KERNEL_VERSION=linux-5.2.12
GLIBC_VERSION=glibc-2.29
MPFR_VERSION=mpfr-4.0.2
GMP_VERSION=gmp-6.1.2
MPC_VERSION=mpc-1.1.0
ISL_VERSION=isl-0.18
CLOOG_VERSION=cloog-0.18.1
PARALLEL_MAKE=-j6
LANGUAGES="c,c++"
export PATH=$INSTALL_PATH/bin:$PATH

mkdir -p $INSTALL_PATH
mkdir -p $DEPENDENCIES_PATH

USE_NEWLIB=0
USE_GCC_PREREQUITES_SCRIPT=0
USE_OPTIONAL_PREREQUITES=0
PACMAN_SUPPORT=1
# Download packages
# IMPLEMENT CHOICE OF USING SCRIPT IN gcc/config/download_prerequisitites
export http_proxy=$HTTP_PROXY https_proxy=$HTTP_PROXY ftp_proxy=$HTTP_PROXY

# In short, if deps/compiler optimzers libs are in GCC dir, they are used
# Step 1. Binutils
# ADD CHECK IF DOWNLOADED AND BUILD TO AVOID NEW DOWNLOAD
cd $DEPENDENCIES_PATH

if [ ! -d ${BINUTILS_VERSION} ]; then
wget -nc https://ftp.gnu.org/gnu/binutils/$BINUTILS_VERSION.tar.gz
tar zxf $BINUTILS_VERSION.tar.gz
rm -rf $BINUTILS_VERSION.tar.gz
mkdir build-binutils && cd build-binutils
../$BINUTILS_VERSION/configure \
                    --prefix=$INSTALL_PATH \
                    --target=$TARGET \
                    --disable-nls # tells binutils not to include native language support. 
make ${PARALLEL_MAKE}
make install
fi
echo Binutils built and installed.

# Step 2. Linux Kernel Headers
cd $DEPENDENCIES_PATH
if [ $USE_NEWLIB -ne 0 ]; then
    wget -nc -O newlib-master.zip https://github.com/bminor/newlib/archive/master.zip || true
    unzip -qo newlib-master.zip
fi
if [ ! -d $LINUX_KERNEL_VERSION ]; then
    wget -nc https://www.kernel.org/pub/linux/kernel/v5.x/$LINUX_KERNEL_VERSION.tar.xz
    tar xf ${LINUX_KERNEL_VERSION}.tar.xz
    rm -rf  ${LINUX_KERNEL_VERSION}.tar.xz
    cd $LINUX_KERNEL_VERSION
    make ARCH=$LINUX_ARCH INSTALL_HDR_PATH=$INSTALL_PATH/$TARGET headers_install
fi
echo Created linux root with headers.

# Step 3. C/C++ Compilers
# GCC now requires the GMP, MPFR and MPC packages. 
# As these packages may not be included in your host distribution, they will be built with GCC. 
# Unpack each package into the GCC source directory and rename the resulting directories so the GCC build procedures will automatically use them: 

 #--disable-decimal-float, --disable-threads, --disable-libatomic, --disable-libgomp, --disable-libquadmath, --disable-libssp, --disable-libvtv, --disable-libstdcxx

#    These switches disable support for the decimal floating point extension, threading, libatomic, libgomp, libquadmath, libssp, libvtv, and the C++ standard library respectively. These features will fail to compile when building a cross-compiler and are not necessary for the task of cross-compiling the temporary libc.


#    --with-sysroot=$LFS                            \
# REDUCE DISABLE STUFF IF LIBS WONT COMPILE, MOTIVATION WAS
# THAT THE HALF-MADE COMPILE MADE TO MAKE BOOTSTRAP COMPILER, DIDN'T NEED ALL THIS
# AND WAS ADDED LATER IN STEPS 4-6
cd $DEPENDENCIES_PATH
if [ ! -d ${GCC_VERSION} ]; then
    wget -nc https://ftp.gnu.org/gnu/gcc/$GCC_VERSION/$GCC_VERSION.tar.gz
    tar xf $GCC_VERSION.tar.gz
    rm -rf $GCC_VERSION.tar.gz
    # If GCC PREREQUITES where downloaded with custom version, 
    # Do manual symbolic links: https://gcc.gnu.org/install/download.html
        # TEST THIS MANUALLY WITH EXIT TO SE SYMLINK IS CORRECT
    if [ $USE_GCC_PREREQUITES_SCRIPT -ne 0 ]; then
        wget -nc https://ftp.gnu.org/gnu/mpfr/$MPFR_VERSION.tar.xz
        wget -nc https://ftp.gnu.org/gnu/gmp/$GMP_VERSION.tar.xz
        wget -nc https://ftp.gnu.org/gnu/mpc/$MPC_VERSION.tar.gz
        ln -sf `ls -1d mpfr-*/` ${GCC_VERSION}/mpfr
        ln -sf `ls -1d gmp-*/` ${GCC_VERSION}/gmp
        ln -sf `ls -1d mpc-*/` ${GCC_VERSION}/mpc
    else
        cd $GCC_VERSION 
        ./contrib/download_prerequisites
        cd $DEPENDENCIES_PATH
    fi 
    if [ $USE_OPTIONAL_PREREQUITES -ne 0 ]; then
        wget -nc ftp://gcc.gnu.org/pub/gcc/infrastructure/$ISL_VERSION.tar.bz2
        wget -nc ftp://gcc.gnu.org/pub/gcc/infrastructure/$CLOOG_VERSION.tar.gz
        ln -sf `ls -1d isl-*/` ${GCC_VERSION} isl
        ln -sf `ls -1d cloog-*/` ${GCC_VERSION} cloog
    fi
    mkdir -p build-gcc && cd build-gcc
    ../$GCC_VERSION/configure              \
        --prefix=$INSTALL_PATH             \
        --target=$TARGET                   \
        --disable-nls                      \
        --with-arch=${TARGET_ARCHITECTURE} \
        --enable-languages=$LANGUAGES
    make $PARALLEL_MAKE all-gcc
    make $PARALLEL_MAKE install-gcc
fi
# DOWNLOADS MPFR, GMP, MPC, ISL for that GCC and symlincs automaticly



# Step 4. Standard C library headers
cd $DEPENDENCIES_PATH

if [ ! -d ${GLIBC_VERSION} ]; then
    wget -nc https://ftp.gnu.org/gnu/glibc/$GLIBC_VERSION.tar.xz
    tar -xf ${GLIBC_VERSION}.tar.xz
    rm -rf ${GLIBC_VERSION}.tar.xz
    mkdir -p build-glibc
    cd build-glibc
    ../${GLIBC_VERSION}/configure \
                    --prefix=$INSTALL_PATH/$TARGET \
                    --build=$MACHTYPE \
                    --host=$TARGET \
                    --target=$TARGET \
                    --with-headers=$INSTALL_PATH/$TARGET/include \
                    --disable-multilib \
                    libc_cv_forced_unwind=yes

    make install-bootstrap-headers=yes install-headers
    make $PARALLEL_MAKE csu/subdir_lib
    install csu/crt1.o csu/crti.o csu/crtn.o $INSTALL_PATH/$TARGET/lib
    $TARGET-gcc -nostdlib -nostartfiles -shared -x c /dev/null -o $INSTALL_PATH/$TARGET/lib/libc.so
    touch $INSTALL_PATH/$TARGET/include/gnu/stubs.h

    # Steps 5,6,7 can be outside iftest, just need better logic to check if make install have been run.
    # Step 5.  Compiler support library
    cd $DEPENDENCIES_PATH
    cd build-gcc
    make ${PARALLEL_MAKE} all-target-libgcc
    make ${PARALLEL_MAKE} install-target-libgcc

    # Step 6. Standard C Library & rest of Glibc
    cd $DEPENDENCIES_PATH
    cd build-glibc
    make ${PARALLEL_MAKE}
    make ${PARALLEL_MAKE} install

    cd $DEPENDENCIES_PATH
    cd build-gcc
    make ${PARALLEL_MAKE} all
    make install

    #gcc-9.1.0/libsanitizer/asan/asan_linux.cc 
    # add #include <linux/limits.h> line 57
    rm -rf build-gcc build-glib build-binutils
    rm -rf $BINUTILS_VERSION $GCC_VERSION $LINUX_KERNEL_VERSION $GLIBC_VERSION
fi
    echo DONE



if [ ${PACMAN_SUPPORT} -ne 0 ]; then
    PACMAN_VERSION=pacman-5.1.3
    if [ ! -d ${PACMAN_VERSION} ]; then
    cd ${DEPENDENCIES_PATH}
    wget https://sources.archlinux.org/other/pacman/${PACMAN_VERSION}.tar.gz \
    --directory-prefix=${DEPENDENCIES_PATH} 

    tar zxf ${PACMAN_VERSION}.tar.gz
    
    if [ ! -d ${TARGET_ROOT}/pacman_root ]; then
        mkdir ${TARGET_ROOT}/pacman_root
    fi

    cd ${PACMAN_VERSION}
    ./configure --prefix=${INSTALL_PATH} \
        --exec-prefix=${INSTALL_PATH} \
        --with-root-dir=${TARGET_ROOT}/pacman_root
    make ${PARALLEL_MAKE} install

    #sed -e "s|TARGET_ROOT|$INSTALL_PATH|g" ${CHINX_DIR}/pacman_support/pacman_sketch.conf > ${INSTALL_PATH}/etc/pacman.conf

    cp ${CHINX_DIR}/pacman_support/makepkg_arm8.conf  ${INSTALL_PATH}/etc/makepkg.conf
    tail -n 25 ${CHINX_DIR}/pacman_support/pacman_sketch.conf | \
        sed -e "s|TARGET_ROOT|$INSTALL_PATH/pacman_root|g" >> ${INSTALL_PATH}/etc/pacman.conf
    sed -i -e 's/auto/aarch64/g' ${INSTALL_PATH}/etc/pacman.conf
    
    if [ ! -d ${INSTALL_PATH}/etc/pacman.d ]; then
        mkdir ${INSTALL_PATH}/etc/pacman.d
    fi

    if [ ! -d ${INSTALL_PATH}/etc/pacman.d ]; then
        mkdir ${INSTALL_PATH}/etc/pacman.d
    fi
    cp ${CHINX_DIR}/pacman_support/mirrorlist ${INSTALL_PATH}/etc/pacman.d/
    sudo pacman-key --init 

    cd ${DEPENDENCIES_PATH}
    if [ ! -d archlinuxarm-keyring ]; then
        git clone https://github.com/archlinuxarm/archlinuxarm-keyring
    fi
    cd archlinuxarm-keyring
    make PREFIX=${INSTALL_PATH} install

    sudo pacman-key --populate archlinuxarm
    sudo pacman -Sy
    # IMPLEMENT SYMLINKING IN THIS WAY:
    # After updating database, make usr/bin, usr/lib in root, then move bin and lib to respectibly folders
    # Now remove bin, lib. 
    # This because arch linux uses this structure, and packages will fail to install if it finds bin or lib
    # in root folder.
    # When installing the first package (can be any), the system will make the symlink themselves
    fi
fi

if [ ! -d ${INSTALL_PATH}/utilities ]; then
    mkdir ${INSTALL_PATH}/utilities
fi
sed "s|INSTALL_PATH|$INSTALL_PATH|g" ${CHINX_DIR}/chinx_sed.sh > ${INSTALL_PATH}/utilities/chinx.sh
cp ${CHINX_DIR}/cmake_support/cross_arm64.cmake ${INSTALL_PATH}/utilities/

echo DONE 
